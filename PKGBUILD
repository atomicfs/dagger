# Maintainer: George Rawlinson <grawlinson@archlinux.org>

pkgname=dagger
pkgver=0.10.0
pkgrel=1
pkgdesc='A portable devkit for CI/CD pipelines'
arch=('x86_64')
url='https://dagger.io'
license=('Apache')
depends=('glibc')
makedepends=('git' 'go')
options=('!lto')
_commit='39f56586c9e005f6d45eb9a3c669d3a3b048fe9a'
source=("$pkgname::git+https://github.com/dagger/dagger.git#commit=$_commit")
b2sums=('SKIP')

pkgver() {
  cd "$pkgname"

  git describe --tags | sed 's/^v//'
}

prepare() {
  cd "$pkgname"

  # create directory for build output
  mkdir build

  # download dependencies
  export GOPATH="${srcdir}"
  go mod download
}

build() {
  cd "$pkgname"

  # set Go flags
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export GOPATH="${srcdir}"

  go build -v \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags "-compressdwarf=false \
    -linkmode external \
    -extldflags ${LDFLAGS} \
    -X github.com/dagger/dagger/engine.Version=$pkgver" \
    -o build \
    ./cmd/...
}

# TODO tests now require docker *kicks docker*
#check() {
#  cd "$pkgname"
#
#  go test -v ./...
#}

package() {
  cd "$pkgname"

  install -vDm755 -t "$pkgdir/usr/bin" build/dagger
}
